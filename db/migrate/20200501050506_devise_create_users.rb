# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[6.0]
  def change
    #### User:ユーザーテーブル（devise）####
    create_table :users do |t|
      #### devise によって追加されないカラム ####
      ## ユニークID
      t.string :uuid, default: "", null: false
      ## ハンドルネーム
      t.string :handle_name, default: "", null: false

      #### devise によって自動で追加されるカラム ####
      ## Database authenticatable
      t.string :email,              default: "", null: false
      t.string :encrypted_password, default: "", null: false

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at
    
      ## Rememberable
      t.datetime :remember_created_at
    
      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip
    
      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email

      #### timestamp ####
      t.timestamps null: false
    end

    ## index
    add_index :users, :uuid,                 unique: true
    add_index :users, :email,                unique: true
    add_index :users, :confirmation_token,   unique: true
    add_index :users, :reset_password_token, unique: true

    #### Role: 権限テーブル ####
    create_table :roles do |t|
      ## 権限名（英語）
      t.string "en_name", default: "", null: false
      ## 権限名（日本語）
      t.string "jp_name", default: "", null: false

      ## timestamps
      t.timestamps
    end

    ## index
    add_index :roles, :en_name, unique: true
    add_index :roles, :jp_name, unique: true

    #### UserRole: ユーザー権限管理テーブル ####
    create_table :user_roles do |t|
      ## ユーザーID
      t.references :user, foreign_key: true, index: true, null: false
      ## 役割ID
      t.references :role, foreign_key: true, index: true, null: false
      ## 登録日時
      t.datetime :registered_at

      ## timestamps
      t.timestamps
    end

    ## index
    add_index :user_roles, %i[user_id role_id], unique: true
  end
end
