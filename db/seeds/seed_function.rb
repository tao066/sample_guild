# frozen_string_literal: true

require "csv"

class SeedFunction
  class << self
    ## `db/csvs` 配下から csv ファイルを読み込む
    def read_csv(filename)
      table = CSV.table(File.join("#{Rails.root}/db/csvs/#{filename}.csv"))
      if block_given?
        table.each {|data| yield data.to_h }
      else
        table.inject([]) {|array, data| array << data.to_h }
      end
    end

    ## csv を読み込んで Model に record を import する
    ## path は `db/csvs` 配下から .csv 抜きで行う
    def read_and_import(model, filepath, &block)
      ## record を入れるための入れ物を作成する
      records = []

      ## input 開始を宣言
      print "%<model>20s: " % { model: model.name.underscore.pluralize }

      ## csv をロードして、records に追加していく
      read_csv(filepath) do |data|
        ## レコードを呼び出す
        record = model.find_by(id: data[:id])

        ## なければ import でまとめて作成する
        ## あるならその場で update する
        if record.nil?
          ## レコードを追加
          records << model.new(data)

          ## 特別な処理があればここで行う
          block.call(records.last) if block_given?

          ## バリデーションに引っかからなければ次
          next if records.last.valid?

          ## 引っ掛かった場合はエラーを出力して終了する
          raise ActiveRecord::RecordInvalid, records.last
        else
          ## 既存のレコードの情報を入れ替え
          record.assign_attributes(data)

          ## 特別な処理があればここで行う
          # block.call(records.last) if block_given?

          ## 変更されたオブジェクトを更新。できなければエラーを吐いてもらう
          ## 新規更新じゃなくても使える
          record.save!
        end
      end

      ## model に records をインポートする
      model.import records if records != []

      ## 処理終了を宣言する
      puts "complete"
    end
  end
end
