# frozen_string_literal: true

require_relative "../seed_function"

## 権限レコードを追加

## init_data
# role
SeedFunction.read_and_import(Role, "init_data/role")

## develop
# user
SeedFunction.read_and_import(User, "develop/user", &:skip_confirmation!)

# user_role
SeedFunction.read_and_import(UserRole, "develop/user_role")
