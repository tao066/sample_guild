# frozen_string_literal: true

## 指定された環境配下にある seed ファイルを読み込む
Dir.glob(File.join("#{Rails.root}/db/seeds/#{Rails.env}/*.rb")).each do |file|
  desc "Load the seed data from db/seeds/#{Rails.env}/#{File.basename(file)}."
  task "db:seed:#{File.basename(file).gsub(/\..+$/, "")}" => :environment do
    load(file)
  end
end
