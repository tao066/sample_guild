# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :init_current_user

  ## 現在のユーザー情報の取得
  ## ログイン時
  ##  * @current_user : ログイン中のユーザー情報
  ##  * @current_role : アクセス中のページの権限
  ##                    コントローラの一つ目のネストで判断する
  ## ログアウト時
  ##  * @current_user : 空のユーザー情報
  ##  * @current_role : `:guest`
  def init_current_user
    if user_signed_in?
      @current_user = current_user
      @current_role = self.class.to_s.split("::").first.underscore.to_sym
    else
      @current_user = User.new
      @current_role = :guest
    end
  end

  ## ログインしていない場合はトップページに移動する
  # def valid_logged_in
  #   unless @current_user.persisted?
  #     flash[:alert] = "ログインまたは登録が必要です。"
  #     redirect_to root_path
  #     return true
  #   end

  #   return false if @current_user.has_role?(@current_role)

  #   flash[:alert] = "権限がありません"
  #   redirect_to @current_user.mypage_path
  # end

  ## ログインしている場合はマイページに移動する
  def valid_not_logged_in
    return false unless @current_user.persisted?

    flash[:alert] = "既にログインしています。"
    redirect_to root_path
  end
end
