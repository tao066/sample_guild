# frozen_string_literal: true

class Mailer::Users::ConfirmationsController < Devise::ConfirmationsController
  ## devise の初期化
  include CustomDeviseMethod

  # before_action :valid_not_logged_in, only: %i[new create]

  # GET /confirmation/new
  # def new
  #   super
  # end

  # POST /confirmation
  # def create
  #   super
  # end

  # GET /confirmation?confirmation_token=abcdef
  def show
    super
  end

  protected

    # user に応じて遷移するページを決める
    def after_confirmation_path_for(resource_name, resource)
      # サインインしていなければサインインする
      # sign_in(resource_name, resource) unless signed_in?(resource_name)

      ## マイページに移動
      # resource.mypage_path

      root_path
    end
end
