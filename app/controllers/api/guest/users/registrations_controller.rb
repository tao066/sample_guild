# frozen_string_literal: true

class Api::Guest::Users::RegistrationsController < Devise::RegistrationsController
  ## devise の初期化
  include CustomDeviseMethod

  before_action :valid_not_logged_in

  ## GET /sign_up
  def new
    ## devise の起動。ここで resource が作成される。
    build_resource

    ## role が保存できるように設定
    resource.roles.build

    ## サインアップ用の role の呼び出し
    @roles = Role.sign_up_roles

    ## role の情報だけ返す
    render json: { roles: @roles }, status: :ok
  end

  ## POST /
  def create
    ## devise の起動。ここで resource が作成される。
    build_resource

    ## User.new(create_user_params) の代わり
    self.resource = resource_class.new(create_user_params)

    if resource.valid?(:sign_up)
      resource.save!
      expire_data_after_sign_in!
      render json: { flash: "アカウントを有効化するためのメールを数分後に配信します。" }, status: :ok
    else
      render json: { errors: resource.column_errors }, status: :unprocessable_entity
    end
  end

  private

    def create_user_params
      params.require(:user).permit(
        { role_ids: [] }, :uuid, :handle_name, :email, :password, :password_confirmation
      )
    end
end
