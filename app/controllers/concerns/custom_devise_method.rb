# frozen_string_literal: true

module CustomDeviseMethod
  extend ActiveSupport::Concern

  def authenticate_user!(opts = {})
    false
  end

  def require_no_authentication
    false
  end
end
