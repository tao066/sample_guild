# frozen_string_literal: true

class Guest::HomeController < ApplicationController
  def index
  end
end
