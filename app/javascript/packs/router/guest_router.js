// require vue
import Vue       from 'vue/dist/vue.esm.js'
import VueRouter from 'vue-router'

// require components
import Top    from '../components/guest/top.vue'
import SignUp from '../components/guest/sign_up.vue'
import SignIn from '../components/guest/sign_in.vue'

// require router
Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  routes: [
    { path: '/',        component: Top },
    { path: '/sign_up',   component: SignUp },
    { path: '/sign_in', component: SignIn },
  ],
})
