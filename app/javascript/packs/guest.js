import Vue    from 'vue/dist/vue.esm.js'
import Router from './router/guest_router'
import Header from './components/header/guest.vue'

var app = new Vue({
  router: Router,
  el: '#app',
  components: {
    'navbar': Header,
  }
});
