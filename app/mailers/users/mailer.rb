# frozen_string_literal: true

class Users::Mailer < Devise::Mailer
  default from: "guild.automailer@sample.com"

  # gives access to all helpers defined within `application_helper`.
  helper :application

  # Optional. eg. `confirmation_url`
  include Devise::Controllers::UrlHelpers

  layout "mailer"

  def confirmation_instructions(record, token, opts = {})
    @user  = record
    @token = token

    subject = if record.unconfirmed_email.nil?
                "【Guild】会員登録完了メール"
              else
                "【Guild】メールアドレス変更手続きを完了してください"
              end

    mail(subject: subject, to: @user.email)
  end

  def reset_password_instructions(record, token, opts = {})
    @user  = record
    @token = token

    mail(subject: "【Guild】パスワード変更手続きを完了してください", to: @user.email)
  end

  def password_change(record, opts = {})
    @user = record

    mail(subject: "【Guild】パスワードが変更されました", to: @user.email)
  end
end
