# frozen_string_literal: true

class User < ApplicationRecord
  include Rails.application.routes.url_helpers

  ## 入れてないモジュール
  ## :lockable, :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :timeoutable, :trackable

  #### 紐付け ####
  ## 権限の紐付け
  has_many :user_roles, dependent: :destroy
  has_many :roles, through: :user_roles

  #### バリデーション ####
  ## 基本
  validates :uuid, allow_blank: true, length: { in: 2..30 },
                   format: { with: /\A[a-z0-9]+\z/, message: "は半角英小文字と半角数字のみを入力してください" }
  validates :uuid, presence: true, uniqueness: { case_sensitive: true }
  validates :handle_name, allow_blank: true, length: { maximum: 255 }
  validates :handle_name, presence: true

  ## web 上でサインアップする時に実行
  with_options on: :sign_up do
    validate :add_role_to_sign_up
  end

  #### 権限に関する関数 ####
  ## 対象の権限を持っているかどうか検証する
  def has_role?(symbol)
    @en_roles ||= roles.pluck(:en_name)
    @en_roles.include?(symbol.to_s)
  end

  ## 対象の role を呼び出す
  def role(symbol)
    @roles ||= {}
    @roles[symbol] ||= Role.find_by(en_name: symbol.to_s)
  end

  ## 対象の user_role を呼び出す
  def user_role(symbol)
    @user_role ||= {}

    if @user_role[symbol].nil? && !role(symbol).nil?
      @user_role[symbol] = UserRole.find_by(user_id: id, role_id: role(symbol))
    end

    @user_role[symbol]
  end

  private

    ## 以下の条件に合っていない場合はフォームエラーを出力する
    ##  * 権限が 1 つだけ選択されていること
    ##  * 指定されている権限が engineer または client であること
    def add_role_to_sign_up
      if roles.size != 1
        errors.add(:roles, "を選択してください")
      elsif roles.first.id != 2 && roles.first.id != 3
        errors.add(:roles, "に有効ではない値が設定されています")
      end
    end
end
