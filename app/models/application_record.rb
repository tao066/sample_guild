# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def column_errors
    err = {}

    if errors.any?
      keys = errors.messages.reject {|_, val| val == [] }.keys
      keys.each do |k|
        err.store(self.class.name.underscore + "_" + k.to_s.split(".").join("_attributes_"), errors.full_messages_for(k))
      end
    end

    err
  end
end
