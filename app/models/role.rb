# frozen_string_literal: true

class Role < ApplicationRecord
  has_many :user_roles, dependent: :destroy
  has_many :users, through: :user_roles

  validates :en_name, presence: true, uniqueness: { case_sensitive: true },
                      length: { maximum: 255 }, format: { with: /\A[a-z]+\z/ }

  validates :jp_name, presence: true, uniqueness: { case_sensitive: true },
                      length: { maximum: 255 }

  #### 権限に関する関数 ####
  ## サインアップ用のロールを出力する
  def self.sign_up_roles
    where.not(id: 1)
  end
end
