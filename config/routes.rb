# frozen_string_literal: true

Rails.application.routes.draw do
  ## Skip all Devise default paths
  devise_for :users, path: "", skip: :all

  ## root path
  root to: "guest/home#index"

  ## guest page
  get "/sign_up", to: "guest/home#index"
  get "/sign_in", to: "guest/home#index"

  ## メール認証の受け取り
  namespace :mailer do
    devise_scope :user do
      scope module: :users do
        resource :user_confirmation, only: :show, path: :confirmation, controller: :confirmations
      end
    end
  end

  ## Api
  namespace :api, format: :json do
    ## guest api
    namespace :guest do
      ## アカウント機能
      devise_scope :user do
        scope module: :users do
          # registrations controller
          resource :user_registration, only: %i[new create], path: "", path_names: { new: :sign_up }, controller: :registrations
        end
      end
    end
  end
end
